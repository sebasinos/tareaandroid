package com.example.myapplication;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    private TextView dividendo, divisor, textoResultado;
    private Button dividir, triangulo;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        dividendo = findViewById(R.id.dividendo);
        divisor = findViewById(R.id.divisor);
        dividir = findViewById(R.id.bDividir);
        triangulo = findViewById(R.id.btriangulo);
        textoResultado = findViewById(R.id.tResultado);


        dividir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int resultado = Integer.parseInt(dividendo.getText().toString()) / Integer.parseInt(divisor.getText().toString());
                textoResultado.setText(String.valueOf(resultado));

            }
        });

        triangulo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent( MainActivity.this, Main2Activity.class);
                startActivity(intent);

            }
        });


    }
}
