package com.example.myapplication;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class Main2Activity extends AppCompatActivity {
    private TextView ladoA, ladoB, ladoC, textoResultado;
    private Button calcular;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        ladoA = findViewById(R.id.ladoA);
        ladoB = findViewById(R.id.ladoB);
        ladoC = findViewById(R.id.ladoC);
        textoResultado = findViewById(R.id.result);
        calcular= findViewById(R.id.calcular);

        calcular.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (ladoA.getText().toString().isEmpty()){
                    textoResultado.setText(String.valueOf("Ingrese Valor"));
                }
                else if (ladoB.getText().toString().isEmpty()){
                    textoResultado.setText(String.valueOf("Ingrese Valor"));
                }
                else if (ladoC.getText().toString().isEmpty()) {
                    textoResultado.setText(String.valueOf("Ingrese Valor"));

                }

                else {
                    int la = Integer.parseInt(ladoA.getText().toString());
                    int lb = Integer.parseInt(ladoB.getText().toString());
                    int lc = Integer.parseInt(ladoC.getText().toString());

                    if (la + lb > lc && la + lc > lb && lb + lc > la) {
                        if (la == lb && la == lc) {
                            textoResultado.setText(String.valueOf("Triangulo Equilatero"));

                        } else if (la != lb && la != lc && lb != lc) {
                            textoResultado.setText(String.valueOf("Triangulo Escaleno"));
                        } else {
                            textoResultado.setText(String.valueOf("Triangulo Isoceles"));
                        }

                    } else {
                        textoResultado.setText(String.valueOf("No es Triangulo"));

                    }

                }
            }
        });
    }
}
